# Bam2Fastq

## Contents

- [Sources](#sources)
- [SAMtools](#Samtools)
- [Bedtools](#Bedtools)
- [Picard](#Picard)


## Sources

* <http://www.htslib.org/doc/samtools-fasta.html>
* <http://bedtools.readthedocs.org/en/latest/content/tools/bamtofastq.html>
* <http://broadinstitute.github.io/picard/command-line-overview.html#SamToFastq>

##  Tools installation using conda 
    conda install -c bioconda/label/cf201901 samtools
    conda install -c bioconda/label/cf201901 bamtools
    conda install -c bioconda/label/cf201901 picard


## Samtools


    samtools sort paired read alignment .bam file (sort by name -n)
    #for single end sequenced data
    samtools bam2fq SAMPLE.bam > SAMPLE.fastq
    #for paired-end sequenced data
    samtools fastq -@ 8 SAMPLE_sorted.bam -1 SAMPLE_R1.fastq.gz -2 SAMPLE_R2.fastq.gz

## Bedtools


    #for single end sequenced data
    bedtools bamtofastq -i NA18152.bam -fq NA18152.fq
    
    #for paired-end sequenced data
    samtools sort -n -o aln.qsort.bam aln.bam
    bedtools bamtofastq -i aln.qsort.bam -fq aln.end1.fq -fq2 aln.end2.fq
    
    #Using GNU parallel: 
    ls *.bam | parallel -j 2 "bedtools bamtofastq {} > {.}.fastq"

## Picard

    
    java -Xmx2g -jar Picard/SamToFastq.jar I=SAMPLE.bam F=SAMPLE_r1.fastq F2=SAMPLE_r2.fastq


## extracting forward reads and reverse reads 
[[back to top](#contents)]

    cat SAMPLE.fastq | grep '^@.*/1$' -A 3 --no-group-separator > SAMPLE_r1.fastq
    
    cat SAMPLE.fastq | grep '^@.*/2$' -A 3 --no-group-separator > SAMPLE_r2.fastq
