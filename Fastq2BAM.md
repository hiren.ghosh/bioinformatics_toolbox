# Mapping Reads on a Genomic Sequence: 
#Mapping reads with bwa and bowtie

We'll map the sequencing data to the ancestral reference genome in this lesson. We'll map the reads using the evolved line's quality trimmed forward and reverse DNA sequences using a tool called BWA.

## Contents

- [Sources](#sources)


## Sources

## Installing the software

We will use here BWA to map our reads to our genome. BWA can map single- or paired-end sequencing data to a reference genome [LI2009]. It needs an indexing stage where the reference genome is supplied and BWA creates an index that is used to align the reads to the reference genome. While this method is time consuming, the index may be reused. The BWA tools' general command structure is illustrated below:
It is simple to install and use.

    $conda create --yes -n mapping samtools bwa qualimap r-base
    $conda activate mapping
    
## indexing and mapping
    
    # bwa index help
    $ bwa index

    # Create an BWA index for user provided reference genome assembly.
    $ bwa index path/to/reference-genome.fa

    # bwa mem help
    $ bwa mem

    # Now that we have created our index, it is time to map the trimmed sequencing reads of our two evolved line to the reference genome.

    # single-end mapping, general command structure, adjust to your case
    $ bwa mem path/to/reference-genome.fa path/to/reads.fq.gz > path/to/aln-se.sam

    # paired-end mapping, general command structure, adjust to your case
    $ bwa mem path/to/reference-genome.fa path/to/read1.fq.gz path/to/read2.fq.gz > path/to/aln-pe.sam

 ## Processing mapped file 
Now that we have created our index, it is time to map the trimmed sequencing reads of our two evolved line to the reference genome. 
we will use here samtools to conver sam to bam file 

    $ samtools sort -n -O sam mappings/evol1.sam | samtools fixmate -m -O bam - mappings/evol1.fixmate.bam

## Sorting
We are going to use SAMtools again to sort the bam-file into coordinate order:

    # convert to bam file and sort
    $ samtools sort -O bam -o mappings/evol1.sorted.bam mappings/evol1.fixmate.bam

    # Once it successfully finished, delete the fixmate file to save space
    $ rm mappings/evol1.fixmate.bam

## Remove duplicates

We now eliminate redundant readings. Duplicates are removed to reduce PCR amplification bias generated during library creation. Notably, this is not always advised. It depends on the study goal. Remove duplicates when calling SNPs, since the tools that call SNPs anticipate it (most tools anyways). For example, RNA-seq, you may not wish to delete duplicates.


    $ samtools markdup -r -S mappings/evol1.sorted.bam mappings/evol1.sorted.dedup.bam

    # if it worked, delete the original file
    $ rm mappings/evol1.sorted.bam

Finally the mapping is done. Now we nedd to create a mapping statics 

## Mapping statistics

    $samtools flagstat evol1.sorted.dedup.bam
    #How many readings overlap the genomic location may be obtained from the sorted bam file.
    $ samtools depth mappings/evol1.sorted.dedup.bam | gzip > mappings/evol1.depth.txt.gz

    #Proportion of the Reads that Mapped to the Reference
    $samtools depth -a file.bam | awk '{c++;s+=$3}END{print s/c}'
    $samtools flagstat file.bam | awk -F "[(|%]" 'NR == 3 {print $2}'
    
## Stats with QualiMap 
To discover biases in the sequencing or mapping of the data, QualiMap evaluates the sequencing alignment data in SAM/BAM files according to the mapped reads' characteristics.  

    $ qualimap bamqc -bam mappings/evol1.sorted.dedup.bam
    # Once finsished open reult page with
    $ firefox mappings/evol1.sorted.dedup_stats/qualimapReport.html

## Extract unmapped reads

    $ samtools view -b -f 4 mappings/evol1.sorted.dedup.bam > mappings/evol1.sorted.unmapped.bam
    # we are deleting the original to save space,
    # however, in reality you might want to save it to investigate later
    $ rm mappings/evol1.sorted.dedup.bam

    # count the unmapped reads
    $ samtools view -c mappings/evol1.sorted.unmapped.bam

 Lets extract the fastq sequence of the unmapped reads for read1 and read2.

    $ samtools fastq -1 mappings/evol1.sorted.unmapped.R1.fastq.gz -2 mappings/evol1.sorted.unmapped.R2.fastq.gz mappings/evol1.sorted.unmapped.bam
    # delete not needed files
    $ rm mappings/evol1.sorted.unmapped.bam   










